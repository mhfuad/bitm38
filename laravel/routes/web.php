<?php

Route::get('/', function () {
    return view('welcome');
});
Route::get('/admin', function () {
    return view('Admin.settings.index');
});

Route::get('/main',function(){
    return view('Admin.index');
});
//about
Route::get('/admin/about',function(){
    return view('Admin.About.index');
});
//setting
Route::get('/admin/setting',function(){
    return view('Admin.settings.index');
});
//awards
Route::get('/admin/awards',function(){
    return view('Admin.Awards.index');
});
//skills
Route::get('/admin/skills',function(){
    return view('Admin.Skills.index');
});
//service
Route::get('/admin/service',function(){
    return view('Admin.Service.index');
});
//posts
Route::get('/admin/posts',function(){
    return view('Admin.Posts.index');
});
//portfolios
Route::get('/admin/portfolios',function(){
    return view('Admin.Portfolios.index');
});
//facts
Route::get('/admin/facts',function(){
    return view('Admin.Facts.index');
});
//experience
Route::get('/admin/experience',function(){
    return view('Admin.Experience.index');
});
//experience
Route::get('/admin/experience/create',function(){
    return view('Admin.Experience.create');
});
//education
Route::get('/admin/education',function(){
    return view('Admin.Education.index');
});
//education create
Route::get('/admin/education/create',function(){
    return view('Admin.Education.create');
});
//hobbies
Route::get('/admin/hobbies',function(){
    return view('Admin.Hobbies.index');
});
//hobbies
Route::get('/admin/hobbies/create',function(){
    return view('Admin.Hobbies.create');
});