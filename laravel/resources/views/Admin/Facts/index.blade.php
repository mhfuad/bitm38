@extends('Admin.layouts.master')
@section('facts','active')
@section('title')
    {{'Facts'}}
@endsection

@section('content')
    <form class="form-horizontal form-validate-jquery" action="#">
        <fieldset class="content-group">
            <legend class="text-bold">Update The fields</legend>

            <div class="form-group">
                <label class="control-label col-lg-3">Title <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <input type="text" name="basic" class="form-control" required="required" placeholder="Title">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">Number of Item <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <input type="text" name="basic" class="form-control" required="required" placeholder="Number of item">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">Basic file uploader <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <input type="file" name="unstyled_file" class="form-control" required="required">
                </div>
            </div>

            <div class="text-right">
                <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
                <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </fieldset>
    </form>
@endsection
