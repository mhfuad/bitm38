@extends('Admin.layouts.master')
@section('education','active')
@section('title')
    {{'Education'}}
@endsection

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">My Educations</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a type="button" href="{{'/admin/education/create'}}" class="btn btn-success">Add New</a></li>
                </ul>
            </div>
        </div>



        <div class="table-responsive">
            <table class="table table-bordered table-lg">
                <tr>
                    <th>Title</th>
                    <th>Institute</th>
                    <th>Result</th>
                    <th>Passing Year</th>
                    <th>Main Subject</th>
                    <th>Education Bord</th>
                    <th>Course Duration</th>
                    <th>Action</th>
                </tr>
                <tbody>
                <tr>
                    <td class="col-md-1 col-sm-1">S,S,C</td>
                    <td class="col-md-1 col-sm-1">Jatrabari</td>
                    <td class="col-md-1 col-sm-1">A+</td>
                    <td class="col-md-1 col-sm-1">2004</td>
                    <td class="col-md-1 col-sm-1">Arts</td>
                    <td class="col-md-1 col-sm-1">Dhaka</td>
                    <td class="col-md-1 col-sm-1">10 years</td>
                    <td class="col-sm-1">
                        <ul class="icons-list">
                            <li><a href="#"><i class="icon-pencil7"></i></a></li>
                            <li><a href="#"><i class="icon-trash"></i></a></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-1 col-sm-1">S,S,C</td>
                    <td class="col-md-1 col-sm-1">Jatrabari</td>
                    <td class="col-md-1 col-sm-1">A+</td>
                    <td class="col-md-1 col-sm-1">2004</td>
                    <td class="col-md-1 col-sm-1">Arts</td>
                    <td class="col-md-1 col-sm-1">Dhaka</td>
                    <td class="col-md-1 col-sm-1">10 years</td>
                    <td class="col-sm-1">
                        <ul class="icons-list">
                            <li><a href="#"><i class="icon-pencil7"></i></a></li>
                            <li><a href="#"><i class="icon-trash"></i></a></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-1 col-sm-1">S,S,C</td>
                    <td class="col-md-1 col-sm-1">Jatrabari</td>
                    <td class="col-md-1 col-sm-1">A+</td>
                    <td class="col-md-1 col-sm-1">2004</td>
                    <td class="col-md-1 col-sm-1">Arts</td>
                    <td class="col-md-1 col-sm-1">Dhaka</td>
                    <td class="col-md-1 col-sm-1">10 years</td>
                    <td class="col-sm-1">
                        <ul class="icons-list">
                            <li><a href="#"><i class="icon-pencil7"></i></a></li>
                            <li><a href="#"><i class="icon-trash"></i></a></li>
                        </ul>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
@endsection
