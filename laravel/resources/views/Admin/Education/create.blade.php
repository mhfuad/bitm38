@extends('Admin.layouts.master')
@section('education','active')
@section('title')
    {{'Education'}}
@endsection

@section('content')
    <form class="form-horizontal form-validate-jquery" action="#">
        <fieldset class="content-group">
            <legend class="text-bold">Update The fields</legend>

            <div class="form-group">
                <label class="control-label col-lg-3">Title <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <input type="text" name="basic" class="form-control" required="required" placeholder="Title">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">Institute<span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <input type="text" name="basic" class="form-control" required="required" placeholder="Institute">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">Result<span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <input type="text" name="basic" class="form-control" required="required" placeholder="Result">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">Passing Year<span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <input type="text" name="basic" class="form-control" required="required" placeholder="Passing Year">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">Main subject<span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <input type="text" name="basic" class="form-control" required="required" placeholder="Main subject">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">Education Bord<span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <input type="text" name="basic" class="form-control" required="required" placeholder="Education Bord">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">Course Duration <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <input type="text" name="basic" class="form-control" required="required" placeholder="Course Duration ">
                </div>
            </div>

            <div class="text-right">
                <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
                <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </fieldset>
    </form>
@endsection
