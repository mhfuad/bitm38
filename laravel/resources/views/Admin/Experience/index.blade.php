@extends('Admin.layouts.master')
@section('experience','active')
@section('title')
    {{'Experience'}}
@endsection

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">My Educations</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a type="button" href="{{'/admin/experience/create'}}" class="btn btn-success">Add New</a></li>
                </ul>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-lg">
                <tr>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
                <tbody>
                <tr>
                    <td class="col-md-1 col-sm-1">Playing</td>
                    <td class="col-md-1 col-sm-1">Play Cricket</td>
                    <td class="col-md-1 col-sm-1"><img src="{{asset('assets/images/placeholder.jpg')}}" class="img-thumbnail img-lg" alt=""></td>
                    <td class="col-sm-1">
                        <ul class="icons-list">
                            <li><a href="#"><i class="icon-pencil7"></i></a></li>
                            <li><a href="#"><i class="icon-trash"></i></a></li>
                        </ul>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
@endsection
