<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <a href="{{'/admin/about'}}" class="media-left"><img src="{{asset('assets/images/placeholder.jpg')}}" class="img-circle img-sm" alt=""></a>
                    <div class="media-body">
                        <span class="media-heading text-semibold">Victoria Baker</span>
                        <div class="text-size-mini text-muted">
                            <i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
                        </div>
                    </div>

                    <div class="media-right media-middle">
                        <ul class="icons-list">
                            <li>
                                <a href="{{'/admin/setting'}}"><i class="icon-cog3"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->
                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                    {{--<li class="active"><a href="{{'/admin'}}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>--}}
                    <li class="@yield('setting')"><a href="{{'/admin/setting'}}"><i class="icon-spinner3 spinner"></i> <span>My Setting</span></a></li>
                    <li class="@yield('about')"><a href="{{'/admin/about'}}"><i class="icon-user-plus"></i> <span>About</span></a></li>
                    <li class="@yield('awards')"><a href="{{'/admin/awards'}}"><i class="icon-gift"></i> <span>Awards</span></a></li>
                    <li class="@yield('skills')"><a href="{{'/admin/skills'}}"><i class="icon-footprint"></i> <span>Skills</span></a></li>
                    <li class="@yield('service')"><a href="{{'/admin/service'}}"><i class="icon-alignment-unalign"></i> <span>Service</span></a></li>
                    <li class="@yield('posts')"><a href="{{'/admin/posts'}}"><i class="icon-magazine"></i> <span>Posts</span></a></li>
                    <li class="@yield('portfolios')"><a href="{{'/admin/portfolios'}}"><i class="icon-task"></i> <span>Portfolios</span></a></li>
                    <li class="@yield('facts')"><a href="{{'/admin/facts'}}"><i class="icon-puzzle2"></i> <span>Facts</span></a></li>
                    <li class="@yield('experience')"><a href="{{'/admin/experience'}}"><i class="icon-thumbs-up2"></i> <span>Experience</span></a></li>
                    <li class="@yield('education')"><a href="{{'/admin/education'}}"><i class="icon-spell-check"></i> <span>Education</span></a></li>
                    <li class="@yield('hobbies')"><a href="{{'/admin/hobbies'}}"><i class="icon-people"></i> <span>Hobbies</span></a></li>
                    {{--<li><a href="#"><i class="icon-stack2"></i> <span>Page layouts</span></a></li>
                    <li><a href="#"><i class="icon-copy"></i> <span>Layouts</span></a></li>
                    <li><a href="#"><i class="icon-droplet2"></i> <span>Color system</span></a></li>
                    <li><a href="#"><i class="icon-stack"></i> <span>Starter kit</span></a></li>
                    <li><a href="changelog.html"><i class="icon-list-unordered"></i> <span>Changelog</span></a></li>
                    <li>
                        <a href="#"><i class="icon-pencil3"></i> <span>Form components</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-footprint"></i> <span>Wizards</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-spell-check"></i> <span>Editors</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-select2"></i> <span>Pickers</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-insert-template"></i> <span>Form layouts</span></a>
                    </li>

                    <li>
                        <a href="#"><i class="icon-grid"></i> <span>Components</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-puzzle2"></i> <span>Content appearance</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-thumbs-up2"></i> <span>Icons</span></a>
                    </li>

                    <li>
                        <a href="#"><i class="icon-indent-decrease2"></i> <span>Sidebars</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-sort"></i> <span>Vertical navigation</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-transmission"></i> <span>Horizontal navigation</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-menu3"></i> <span>Navbars</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-tree5"></i> <span>Menu levels</span></a>
                    </li>

                    <li>
                        <a href="#"><i class="icon-graph"></i> <span>Echarts library</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-statistics"></i> <span>D3 library</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-stats-dots"></i> <span>Dimple library</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-stats-bars"></i> <span>C3 library</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-google"></i> <span>Google visualization</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-map5"></i> <span>Maps integration</span></a>

                    </li>

                    <li>
                        <a href="#"><i class="icon-spinner10 spinner"></i> <span>Velocity animations</span></a>
                    </li>
                    <li><a href="extension_blockui.html"><i class="icon-history"></i> <span>Block UI</span></a></li>
                    <li>
                        <a href="#"><i class="icon-upload"></i> <span>File uploaders</span></a>
                    </li>
                    <li><a href="extension_image_cropper.html"><i class="icon-crop2"></i> <span>Image cropper</span></a></li>
                    <li>
                        <a href="#"><i class="icon-calendar3"></i> <span>Event calendars</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-sphere"></i> <span>Internationalization</span></a>

                    </li>

                    <li>
                        <a href="#"><i class="icon-table2"></i> <span>Basic tables</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-grid7"></i> <span>Data tables</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-alignment-unalign"></i> <span>Data tables extensions</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-versions"></i> <span>Responsive options</span></a>
                    </li>

                    <li>
                        <a href="#"><i class="icon-task"></i> <span>Task manager</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-cash3"></i> <span>Invoicing</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-people"></i> <span>User pages</span></a>
                    </li>

                    <li>
                        <a href="#"><i class="icon-magazine"></i> <span>Timelines</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-lifebuoy"></i> <span>Support</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-search4"></i> <span>Search</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-images2"></i> <span>Gallery</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="icon-warning"></i> <span>Error pages</span></a>
                    </li>
                    --}}

                </ul>
            </div>
        </div>
        <!-- /main navigation -->
    </div>
</div>