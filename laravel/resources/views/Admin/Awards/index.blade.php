@extends('Admin.layouts.master')
@section('awards','active')
@section('title')
    {{'Awards '}}
@endsection

@section('content')
    <p class="content-group-lg">My Awards.</p>
    <form class="form-horizontal form-validate-jquery" action="#">
        <fieldset class="content-group">
            <legend class="text-bold">Update The fields</legend>

            <div class="form-group">
                <label class="control-label col-lg-3">Title <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <input type="text" name="basic" class="form-control" required="required" placeholder="Title">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">Organization<span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <input type="text" name="basic" class="form-control" required="required" placeholder="Organization">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">Description<span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <textarea rows="5" cols="5" name="textarea" class="form-control" required="required" placeholder="Description"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">Location<span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <input type="text" name="basic" class="form-control" required="required" placeholder="Location">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3">Year<span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <input type="text" name="basic" class="form-control" required="required" placeholder="Year">
                </div>
            </div>
            <div class="text-right">
                <button type="reset" class="btn btn-default" id="reset">Reset <i class="icon-reload-alt position-right"></i></button>
                <button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
            </div>
        </fieldset>
    </form>
@endsection
