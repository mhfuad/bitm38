<?php

/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 1/4/2017
 * Time: 9:43 AM
 */

include_once("dbConfig.php");


class db{
    public $conn;
    function __construct()
    {
        try{
            $conn = new PDO("mysql:host="..";dbname=myDB", $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
    }
}