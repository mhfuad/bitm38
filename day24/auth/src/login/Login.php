<?php
namespace App\login;
use PDO;
class Login{
    private $userName;
    private $email;
    private $password;
    private $pdo;

    public function __construct(){
        session_start();
        try{
            $this->pdo = new PDO('mysql:host=localhost;dbname=php38', 'root', '');
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch (PDOException $e){
            echo "Error: ". $e->getMessage();
        }
    }
    public function setData($data=''){
        if(array_key_exists('usernamesignup',$data)){
            $this->userName = $data['usernamesignup'];
        }
        if(array_key_exists('emailsignup',$data)){
            $this->email = $data['emailsignup'];
        }
        if(array_key_exists('passwordsignup',$data)){
            $this->password = $data['passwordsignup'];
        }
        if(array_key_exists('username',$data)){
            $this->userName = $data['username'];
        }
        if(array_key_exists('password',$data)){
            $this->password = $data['password'];
        }
        return $this;
    }
    public function store(){
        $sql = "INSERT INTO `auth`(`id`, `unique_id`, `user_name`, `email`, `password`, `create_at`)
                           VALUES (:id,:uniq,:use,:email,:pas,:creat)";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute(
            array(
                ':id'=>null,
                ':uniq'=>uniqid(),
                ':use'=>$this->userName,
                ':email'=>$this->email,
                ':pas'=>$this->password,
                ':creat'=>date('Y-m-d h:m:s')
            )
        );
        if($stmt){
            $_SESSION['msg']="Register Success.";
            header('location: index.php#login');
        }
    }
    public function login(){
        try{
            $sql = "SELECT * FROM `auth` WHERE (`user_name`='$this->userName' or `email`='$this->userName') AND `password`='$this->password'";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            $arr = $stmt->fetchAll();
            return $arr;

        }catch (\PDOException $e){
            echo "Error:".$e->getMessage();
        }
        //echo $sql = "SELECT * FROM `auth` WHERE (`user_name`='$this->userName' or `email`='$this->userName') AND `password`='$this->password'";
    }
}