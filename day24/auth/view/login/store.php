<?php
include_once("../../vendor/autoload.php");

use App\login\Login;
$obj = new Login();

$data = $obj->setData($_POST)->login();

if(!empty($data)){
    $_SESSION['info']=$data;
    header('location: ../dashbord/index.php');
}