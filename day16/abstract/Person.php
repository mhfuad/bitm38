<?php

/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 1/9/2017
 * Time: 9:41 AM
 */
abstract class Person
{
    public abstract function getName();
    protected abstract function getNumberOfPerson();
    public function getClassName(){
        echo "<br>".__CLASS__;
    }
}
class People extends Person{
    public function getName()
    {
        // TODO: Implement getName() method.
        echo "Name is fuad";
    }
    public function getNumberOfPerson()
    {
        // TODO: Implement getNumberOfPerson() method.
        echo "<br>20";
    }
}

$person = new People();
$person->getName();
$person->getNumberOfPerson();
$person->getClassName();
