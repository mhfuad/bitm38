<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 12/12/2016
 * Time: 9:22 AM
 */

$str     = "Line 1\nLine 2\rLine 3\r\nLine 4\n";
$order   = array("\r\n", "\n", "\r");
$replace = '<br />';

// Processes \r\n's first so they aren't converted twice.
echo  str_replace($order, $replace, $str);