<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 12/12/2016
 * Time: 9:25 AM
 */

$input = "Alien";
echo str_pad($input, 10)."<br>";                      // produces "Alien     "
echo str_pad($input, 10, "-=", STR_PAD_LEFT)."<br>";  // produces "-=-=-Alien"
echo str_pad($input, 10, "_", STR_PAD_BOTH)."<br>";   // produces "__Alien___"
echo str_pad($input,  6, "___")."<br>";               // produces "Alien_"
echo str_pad($input,  3, "*")."<br>";                 // produces "Alien"
echo str_pad($input,  50, "$")."<br>";                 // produces "Alien"
echo str_pad($input,  50, "Bangladesh")."<br>";                 // produces "Alien"