<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 19-Dec-16
 * Time: 7:47 AM
 */

$str = "Warning array to string conversion ";
$code = crc32($str);
echo $code;

$str = crc32("Hello world!");
echo 'Without %u: '.$str."<br>";
echo 'With %u: ';
printf("%u",$str);

$str = crc32("Hello world.");
echo 'Without %u: '.$str."<br>";
echo 'With %u: ';
printf("%u",$str);