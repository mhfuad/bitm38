<?php
include_once("../../../../vendor/autoload.php");
use App\Bitm\SEIP\Students\Students;
$stu = new Students();
if(isset($_POST['title'])){
    if(!empty($_POST['title'])){
        if(preg_match("/([a-z*0-9_A-Z])/",$_POST['title'])){
            filter_var($_POST['title'], FILTER_SANITIZE_STRING  );
            $stu->setData($_POST)->store();
        }else{
            $_SESSION['message'] = "Invalid input.";
            header('location: create.php');
        }
    }else{
        $_SESSION['message'] = "feald must not be empty.";
        header('location: create.php');
    }
}

