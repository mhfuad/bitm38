<?php

namespace App\Bitm\SEIP\Students;
use PDO;
class Students{
    public $title;
    public $id;

    public function setData($name = '')
    {
        if(array_key_exists('title',$name)){
            $this->title = $name['title'];
        }
        if(array_key_exists('id',$name)){
            $this->id = $name['id'];
        }
        return $this;
    }

    public function index()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=php38', 'root', '');
            //$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `students`";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            //echo $stmt->rowCount(); // 1
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function store()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=php38', 'root', '');
            $query = "INSERT INTO `students` (`id`, `title`) VALUES (:a, :b)";
            $stmt = $pdo->prepare($query);
            $stmt->execute(
                array(
                    ':a' => null,
                    ':b' => $this->title,
                )
            );
            if($stmt){
                session_start();
                $_SESSION['message'] = "Successfully Submited";
                header('location:index.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this;
    }

    public function show()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=php38', 'root', '');
            //$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "SELECT * FROM `students` WHERE `id`= $this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            //echo $stmt->rowCount(); // 1
            return $stmt->fetch();
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function delete()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=php38', 'root', '');
            //$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "DELETE FROM `students` WHERE `students`.`id` = $this->id";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            if($stmt){
                session_start();
                $_SESSION['message'] = "Successfully Deleted";
                header('location:index.php');
            }
            //echo $stmt->rowCount(); // 1
            //return $stmt->fetch();
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}