<?php
/**
 * array_flip — Exchanges all keys with their associated values in an array
 *
 * Parameters

array
An array of key/value pairs to be flipped.
 */

$a = array("mango", "grab", "blackbary");
$flip = array_flip($a);
echo "<pre>";
print_r($flip);


echo "<br>";
echo "<br>";
echo "<br>";
echo "<br>";
$c = array("bal"=>0,"a"=>1, "b"=>2, "c"=>3);
$flip2 = array_flip($c);
echo "<pre>";
print_r($flip2);