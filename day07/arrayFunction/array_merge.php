<?php
/**
 * array_merge — Merge one or more arrays
 * Parameters ¶

array1
Initial array to merge.

...
Variable list of arrays to merge.
 */

$array1 = array("color" => "red", 2, 4);
$array2 = array("a", "b", "shape" => "trapezoid", 4);
$result = array_merge($array1, $array2);
echo "<pre>";
print_r($result);

echo "<br>";
echo "<br>";
echo "<br>";
echo "<br>";

$array1 = array();
$array2 = array(1 => "data");
$result = array_merge($array1, $array2);
echo "<pre>";
print_r($result);
echo "<br>";
echo "<br>";
echo "<br>";
echo "<br>";


$beginning = 'foo';
$end = array(1 => 'bar');
$result = array_merge((array)$beginning, (array)$end);
print_r($result);
echo "<pre>";
print_r($result);
