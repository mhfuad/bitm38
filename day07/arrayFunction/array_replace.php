<?php
/**
 * array_replace — Replaces elements from passed arrays into the first array
 *
 * Parameters ¶

array1
The array in which elements are replaced.

array2
The array from which elements will be extracted.

...
More arrays from which elements will be extracted. Values from later arrays overwrite the previous values.

*Return Values ¶

Returns an array, or NULL if an error occurs.
 */

$base = array("orange", "banana", "apple", "raspberry");
$replacements = array(0 => "pineapple", 4 => "cherry");
$replacements2 = array(0 => "grape");
echo "<pre>";
print_r($base);
$basket = array_replace($base, $replacements, $replacements2);
echo "<pre>";
print_r($basket);