<?php
/**
 * array_reverse — Return an array with elements in reverse order
 * Parameters ¶

array
The input array.

preserve_keys
If set to TRUE numeric keys are preserved. Non-numeric keys are not affected by this setting and will always be preserved.

Return Values ¶

Returns the reversed array.
 */
$input  = array("php", 4.0, array("green", "red"));
$reversed = array_reverse($input);
$preserved = array_reverse($input, true);
echo "<pre>";
print_r($input);
echo "<pre>";
print_r($reversed);
echo "<pre>";
print_r($preserved);