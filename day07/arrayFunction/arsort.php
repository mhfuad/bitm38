<?php
/**
 * arsort — Sort an array in reverse order and maintain index association
 *
 *
 * Description ¶

bool arsort ( array &$array [, int $sort_flags = SORT_REGULAR ] )
This function sorts an array such that array indices maintain their correlation with the array elements they are associated with.

This is used mainly when sorting associative arrays where the actual element order is significant.

Note:
If two members compare as equal, their relative order in the sorted array is undefined.
Parameters ¶

array
The input array.

sort_flags
You may modify the behavior of the sort using the optional parameter sort_flags, for details see sort().

Return Values ¶

Returns TRUE on success or FALSE on failure.
 */
$fruits = array("d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple");
arsort($fruits);
foreach ($fruits as $key => $val) {
    echo "$key = $val\n";
}