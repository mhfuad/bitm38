<?php
/**
 * array_values — Return all the values of an array
 *
 * Parameters ¶

array
The array.

Return Values ¶

Returns an indexed array of values.
 */
$array = array("size" => "XL", "color" => "gold");
echo "<pre>";
print_r(array_values($array));