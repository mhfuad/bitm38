<?php
/**
 * array_combine — Creates an array by using one array for keys and another for its values
 * Parameters

    keys
    Array of keys to be used. Illegal values for key will be converted to string.

    values
    Array of values to be used
 */

$name = array("fuad","abdullah","abdur rohman");
$age = array(34,6,22);
$combin = array_combine($name, $age);

echo  "<pre>";
    print_r($combin);