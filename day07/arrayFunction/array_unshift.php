<?php
/**
 * array_unshift — Prepend one or more elements to the beginning of an array
 *
 * Parameters ¶

array
The input array.

value1
First value to prepend.

Return Values ¶

Returns the new number of elements in the array.
 */
$queue = array("orange", "banana");
array_unshift($queue, "apple", "raspberry");
echo "<pre>";
print_r($queue);