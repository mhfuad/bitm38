<?php
/**
 * array_pop — Pop the element off the end of array
 */
$stack = array("orange", "banana", "apple", "raspberry");
$fruit = array_pop($stack);
echo "<pre>";
print_r($stack);