<?php
/**
 * array_shift — Shift an element off the beginning of array
 * Description ¶

mixed array_shift ( array &$array )
array_shift() shifts the first value of the array off and returns it, shortening the array
 * by one element and moving everything down. All numerical array keys will be modified
 * to start counting from zero while literal keys won't be touched.

Note: This function will reset() the array pointer of the input array after use.
Parameters ¶

array
The input array.

Return Values ¶

Returns the shifted value, or NULL if array is empty or is not an array.
 */

$stack = array("orange", "banana", "apple", "raspberry");
echo  "<pre>";
print_r($stack);
echo "<br>";
$fruit = array_shift($stack);
echo  "<pre>";
print_r($stack);
$fruit = array_shift($stack);
echo "<br>";
print_r($stack);
echo "<br>";