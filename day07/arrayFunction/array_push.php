<?php
/**
 * Push one or more elements onto the end of array
 *
 * Parameters ¶

array
The input array.

value1
The first value to push onto the end of the array.
 */
$stack = array("orange", "banana");
array_push($stack, "apple", "raspberry");
echo "<pre>";
print_r($stack);