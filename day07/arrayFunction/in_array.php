<?php
/**
 * in_array — Checks if a value exists in an array
 *
 * Description ¶

bool in_array ( mixed $needle , array $haystack [, bool $strict = FALSE ] )
Searches haystack for needle using loose comparison unless strict is set.

Parameters ¶

needle
The searched value.

Note:
If needle is a string, the comparison is done in a case-sensitive manner.
haystack
The array.

strict
If the third parameter strict is set to TRUE then the in_array() function will also check the types of the needle in the haystack.

Return Values ¶

Returns TRUE if needle is found in the array, FALSE otherwise.
 */
$os = array("Mac", "NT", "Irix", "Linux");
if (in_array("Irix", $os)) {
    echo "Got Irix";
}
if (in_array("mac", $os)) {
    echo "Got mac";
}