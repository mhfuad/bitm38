<?php
/**
 * array_pad — Pad array to the specified length with a value
 *
 * Parameters ¶

array
Initial array of values to pad.

size
New size of the array.

value
Value to pad if array is less than size.
 */

$input = array(12, 10, 9);
$result = array_pad($input, 5, 0);

// result is array(12, 10, 9, 0, 0)
echo "<br>";
echo "<pre>";
print_r($result);


$result = array_pad($input, -7, -1);
echo "<br>";
echo "<pre>";
print_r($result);
// result is array(-1, -1, -1, -1, 12, 10, 9)

$result = array_pad($input, 2, "noop");
echo "<br>";
echo "<pre>";
print_r($result);
