<?php
/**
 * array_chunk — Split an array into chunks
 *
 * Parameters

    array
    The array to work on

    size
    The size of each chunk

    preserve_keys
    When set to TRUE keys will be preserved. Default is FALSE which will reindex the chunk numerically
 */

$cars = array("Volvo","Honda","toyota","BMW","Hundey","");
echo "<pre>";
print_r(array_chunk($cars, 4));