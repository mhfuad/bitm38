<?php
/**
 * array_count_values — Counts all the values of an array
 * Parameters

array
The array of values to count
 */

$a = array("fuad","hasan","biswas","fuad",1,1,'c','c');
echo "<pre>";
print_r(array_count_values($a));