<?php
/**
 * array_fill — Fill an array with values
 *
 * Parameters

start_index
The first index of the returned array.

If start_index is negative, the first index of the returned array will be start_index and the following indices will start from zero (see example).

num
Number of elements to insert. Must be greater than or equal to zero.

value
Value to use for filling
 */

$a = array_fill(2,4, "fuad");
echo "<pre>";
print_r($a);