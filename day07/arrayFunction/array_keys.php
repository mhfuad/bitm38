<?php
/**
 * array_keys — Return all the keys or a subset of the keys of an array
 * Parameters ¶

array
An array containing keys to return.

search_value
If specified, then only keys containing these values are returned.

strict
Determines if strict comparison (===) should be used during the search.
 */
$arr = array(0=>100,"green"=>"beautyful");
echo "<pre>";
print_r(array_keys($arr));

echo "<br>";
echo "<br>";
echo "<br>";

$arr = array("blue","green","blue","red","orange","blue");
echo "<pre>";
print_r(array_keys($arr,"blue"));

echo "<br>";
echo "<br>";
echo "<br>";

$arr = array("color"=>array("green","white","orange"),"remark"=>array("good","so much","very much"));
echo "<pre>";
print_r(array_keys($arr));

