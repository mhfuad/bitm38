<?php
/**
 * array_key_exists — Checks if the given key or index exists in the array
 *
 * Parameters ¶

key
Value to check.

array
An array with keys to check.
 */
$a = array("fuad"=>1,"hasan"=>2,"biswas"=>3);
    if (array_key_exists("fuad", $a)){
        echo "fuad key is exist in the array";
    }