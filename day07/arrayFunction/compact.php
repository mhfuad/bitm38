<?php
/**
 * compact — Create array containing variables and their values
 *
 * Description ¶

array compact ( mixed $varname1 [, mixed $... ] )
Creates an array containing variables and their values.

For each of these, compact() looks for a variable with that name in the current symbol table
 * and adds it to the output array such that the variable name becomes the key and the contents of the variable
 * become the value for that key. In short, it does the opposite of extract().

Any strings that are not set will simply be skipped.

Parameters ¶

varname1
compact() takes a variable number of parameters. Each parameter can be either a string containing the name of the variable, or an array of variable names. The array can contain other arrays of variable names inside it; compact() handles it recursively.

Return Values ¶

Returns the output array with all the variables added to it.
 */
$city  = "San Francisco";
$state = "CA";
$event = "SIGGRAPH";

$location_vars = array("city", "state");

$result = compact("event", "nothing_here", $location_vars);
echo "<pre>";
print_r($result);