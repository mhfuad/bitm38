<?php
/**
 * array_unique — Removes duplicate values from an array
 *
 * Parameters ¶

array
The input array.

sort_flags
The optional second parameter sort_flags may be used to modify the sorting behavior using these values:

Sorting type flags:

SORT_REGULAR - compare items normally (don't change types)
SORT_NUMERIC - compare items numerically
SORT_STRING - compare items as strings
SORT_LOCALE_STRING - compare items as strings, based on the current locale.
Return Values ¶

Returns the filtered array.
 */
$input = array("a" => "green", "red", "b" => "green", "blue", "red");
$result = array_unique($input);
echo "<pre>";
print_r($result);