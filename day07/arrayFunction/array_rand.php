<?php
/**
 * array_rand — Pick one or more random entries out of an array
 *
 * Parameters ¶

array
The input array.

num
Specifies how many entries should be picked.



 */
$input = array("Neo", "Morpheus", "Trinity", "Cypher", "Tank");
$rand_keys = array_rand($input,2);
echo "<pre>";
print_r($rand_keys);
echo $input[$rand_keys[0]] . "<br>";
echo $input[$rand_keys[1]] . "<br>";