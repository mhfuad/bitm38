<?php
include_once("../../vendor/autoload.php");
use App\User\User;

$usr = new User();
$arr = $usr->setData($_GET)->show();


?>
<h1>Edit Page</h1>
<a href="index.php">View All</a>
<table >
    <tr>
        <td>Name</td>
        <td>Password</td>
        <td>Email</td>
        <td>Gender</td>
        <td colspan="2">Action</td>
    </tr>
    <?php

    foreach($arr as $a){
    ?>
        <form action="update.php" method="post">
            <tr>
                <input type="hidden" name="id" value="<?php echo $a['unique_id'] ?>">
                <td><input type="text" name="title" value="<?php echo $a['title'] ?>"></td>
                <td><input type="text" name="password" value="<?php echo $a['password'] ?>"></td>
                <td><input type="email" name="email" value="<?php echo $a['email'] ?>"></td>
                <td><input type="text" name="gender" value="<?php echo $a['gender'] ?>"></td>
                <td><input type="submit"></td>
            </tr>
        </form>
    <?php
    }
    ?>
</table>

