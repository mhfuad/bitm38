<?php
include_once("../../vendor/autoload.php");
use App\User\User;

$usr = new User();

//pagination
$totalRows = $usr->getNumberOfRows();
$numerOfItemPerPage = 10;
$totalPage = ($totalRows / $numerOfItemPerPage);
$totalPage = ceil($totalPage);
if(isset($_GET['page'])){
    $correntPage = $_GET['page']-1;
}else{
    $correntPage = 0;
}
$Offset = $correntPage * $numerOfItemPerPage;


$arr = $usr->index($numerOfItemPerPage,$Offset);
if(isset($_SESSION['message'])){
    echo $_SESSION['message']."<br>";
    session_destroy();
} ?>
<form action="search.php" method="get">
    <table>
        <tr>
            <td>Search:</td>
            <td><input type="text" name="keyword" autofocus></td>
            <td><input type="submit" value="Serch"></td>
        </tr>
    </table>
</form>
<h1>Index page</h1>
<a href="create.php">Add New</a>|
<a href="trashList.php">View Delete List</a>|
<a href="report/pdf.php">PDf</a>|
<a href="report/xl.php">XL</a>|
<a href="report/email.php">Email</a>
<table border="1">
    <tr>
        <td>Number</td>
        <td>Id</td>
        <td>Name</td>
        <td>Password</td>
        <td>Email</td>
        <td>Gender</td>
        <td colspan="2">Action</td>
    </tr>
    <?php
    $i = $Offset+1;
    foreach($arr as $a){

    ?>
    <tr>
        <td><?php echo $i++; ?></td>
        <td><?php echo $a['id'] ?></td>
        <td><?php echo $a['title'] ?></td>
        <td><?php echo $a['password'] ?></td>
        <td><?php echo $a['email'] ?></td>
        <td><?php echo $a['gender'] ?></td>
        <td>
            <a href="show.php?id=<?php echo   $a['unique_id'] ?>">View</a>  |
            <a href="edit.php?id=<?php echo   $a['unique_id'] ?>">Edit</a>|
            <a href="trash.php?id=<?php echo $a['unique_id'] ?>">Delete</a>
        </td>
    </tr>
    <?php
    }
    ?>
</table>
<?php for($i = 1; $i<=$totalPage;$i++){?>
    <a href="index.php?page=<?php echo $i+1 ?>"><?php echo $i+1 ?> &nbsp;</a>
<?php }?>