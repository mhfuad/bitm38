<?php
include_once("../../vendor/autoload.php");
use App\User\User;

$usr = new User();
$arr = $usr->trashList();

if(isset($_SESSION['message'])){
    echo $_SESSION['message']."<br>";
    session_destroy();
} ?>
<a href="index.php">Show Original List</a>

<h1>Delete List</h1>
<table border="1">
    <tr>
        <td>Name</td>
        <td>Password</td>
        <td>Email</td>
        <td>Gender</td>
        <td colspan="2">Action</td>
    </tr>
    <?php

    foreach($arr as $a){
        ?>
        <tr>
            <td><?php echo $a['title'] ?></td>
            <td><?php echo $a['password'] ?></td>
            <td><?php echo $a['email'] ?></td>
            <td><?php echo $a['gender'] ?></td>
            <td>
                <a href="show.php?id=<?php echo   $a['unique_id'] ?>">View Details</a>  |
                <a href="edit.php?id=<?php echo   $a['unique_id'] ?>">Edit </a>|
                <a href="restore.php?id=<?php echo   $a['unique_id'] ?>">Restore </a>|
                <a href="delete.php?id=<?php echo $a['unique_id'] ?>">Delete</a>
            </td>
        </tr>
        <?php
    }
    ?>
</table>