<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Dhaka');

if (PHP_SAPI == 'cli') {
    die('This example should only be run from a Web Browser');
}

include_once ("../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php");
include_once ("../../../vendor/autoload.php");
use App\user\User;

$pdf =new  mPDF();
$usr = new User();

$arr = $usr->index();
$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'SL')
    ->setCellValue('B2', 'Id!')
    ->setCellValue('C1', 'Title')
    ->setCellValue('D2', 'Password')
    ->setCellValue('E2', 'Mail')
    ->setCellValue('F2', 'Gender');

$counter = 2;
$serial = 0;


foreach($arr as $a){
    $serial ++;
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$counter,$serial)
        ->setCellValue('B'.$counter,$a['title'])
        ->setCellValue('B'.$counter,$a['password'])
        ->setCellValue('B'.$counter,$a['email'])
        ->setCellValue('B'.$counter,$a['gender']);
    $counter++;
}


$objPHPExcel->getActiveSheet()->setTitle('User List');
$objPHPExcel->setActiveSheetIndex(0);


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="01simple.xlsx"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
