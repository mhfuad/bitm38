<?php
include_once("../../vendor/autoload.php");
use App\User\User;

$usr = new User();
$arr = $usr->setData($_GET)->show();


?>
<h1>View Page</h1>
<a href="index.php">View All</a>
<table border="1">
    <tr>
        <td>Name</td>
        <td>Password</td>
        <td>Email</td>
        <td>Gender</td>
        <td colspan="2">Action</td>
    </tr>
    <?php

    foreach($arr as $a){
        ?>
        <tr>
            <td><?php echo $a['title'] ?></td>
            <td><?php echo $a['password'] ?></td>
            <td><?php echo $a['email'] ?></td>
            <td><?php echo $a['gender'] ?></td>
            <td>
                <a href="edit.php?id=<?php echo   $a['unique_id'] ?>">Edit</a>  |
                <a href="trash.php?id=<?php echo $a['unique_id'] ?>">Delete</a>
            </td>
        </tr>

        <?php
    }
    ?>
</table>

