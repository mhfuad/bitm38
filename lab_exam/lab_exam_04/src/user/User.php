<?php

namespace App\user;
use PDO;

class User
{
    public $id;
    public $title;
    public $password;
    public $email;
    public $gender;
    public $keyword;

    public $pdo ='';

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=php38', 'root', '');

    }
    public function setData($name = ''){
        if(array_key_exists('keyword',$name) ){
            $this->keyword = $name['keyword'];
        }
        if(array_key_exists('id',$name)){
            $this->id = $name['id'];
        }
        if(array_key_exists('title',$name)){
            $this->title = $name['title'];
        }
        if(array_key_exists('password',$name)){
            $this->password = $name['password'];
        }
        if(array_key_exists('email',$name)){
            $this->email = $name['email'];
        }
        if(array_key_exists('gender',$name)){
            $this->gender = $name['gender'];
        }

        return $this;
    }
    public function getNumberOfRows()
    {
        try {
            $query = "SELECT * FROM `users` WHERE `delete_at`='0000-00-00 00:00:00' ";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            return count($stmt->fetchAll());
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function index($numberOfItemPerPage='',$offset=''){
        try {
            $query = "SELECT * FROM `users` WHERE `delete_at`='0000-00-00 00:00:00' LIMIT $numberOfItemPerPage OFFSET $offset";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function store(){
        try {

            $query = "INSERT INTO `users` (`id`,`unique_id`,`title`, `password`, `email`, `gender`)
                  VALUES (:id,:unique, :nam, :pass, :mail, :gender)";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => null,
                    ':unique' => uniqid(),
                    ':nam' => $this->title,
                    ':pass' => $this->password,
                    ':mail' => $this->email,
                    ':gender' => $this->gender,
                )
            );
           if($stmt){

                $_SESSION['message'] = "Insert Successfully";
                header('location: index.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this;
    }
    public function show(){
        try {

            $query = "SELECT * FROM `users` WHERE `unique_id`=:id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id'=>$this->id,
                )
            );
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function update()
    {
        try {
            $query = "UPDATE `users` SET `title`=:title,`password`=:pass,`email`=:email,`gender`=:gender WHERE `unique_id` = :id";
            $stmt = $this->pdo->prepare($query);
               $stmt->execute(
                    array(
                        ':id'=>$this->id,
                        ':title'=>$this->title,
                        ':pass'=>$this->password,
                        ':email'=>$this->email,
                        ':gender'=>$this->gender,
                    )
               );
                if($stmt){
                    $_SESSION['message'] = "Update Successfully";
                    header('location: index.php');
                }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function delete()
    {
        try {
            $query = "DELETE FROM `users` WHERE `unique_id` = :id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id'=>$this->id,
                )
            );
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        if($stmt){
            $_SESSION['message'] = "Delete Successfully";
            header('location: index.php');
        }
    }
    public function trash()
    {
        try {
            $query = "UPDATE `users` SET `delete_at` = :time WHERE `unique_id` = :id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id'=>$this->id,
                    ':time' => date('Y-m-d H:m:s'),
                )
            );
            if($stmt){
                $_SESSION['message'] = "Update Successfully";
                header('location: trashList.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function trashList()
    {
        try {

            $query = "SELECT * FROM `users` WHERE `delete_at`!='0000-00-00 00:00:00'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function restore()
    {
        try {
            $query = "UPDATE `users` SET `delete_at`='0000-00-00 00:00:00' WHERE `unique_id` = :id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id'=>$this->id,
                )
            );
            if($stmt){
                $_SESSION['message'] = "Restore Successfully";
                header('location: index.php');
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function search(){
       // return $this->keyword;
        try {

            $query = "SELECT * FROM `users` WHERE `title`=:title and `delete_at`='0000-00-00 00:00:00'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':title'=> $this->keyword,
                )
            );
            if($stmt){
                return $stmt->fetchAll();
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}






