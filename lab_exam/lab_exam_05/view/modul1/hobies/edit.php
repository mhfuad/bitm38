<?php
include_once ("../../../vendor/autoload.php");

use App\modul1\hobies\Hobies;
$obj =new Hobies();
$arr = $obj->setData($_GET)->show();
/*echo "<pre>";
print_r($arr);
die();*/
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Lab exam 5</title>
</head>
<body>
<h1>Update Page</h1>
<a href="./index.php">Show all list</a>
<form action="update.php" method="post">
    <input type="hidden" name="id" value="<?php echo $arr[0]['unique_id']?>">
    <table border="1">
        <tr>
            <td>Name:</td>
            <td><input type="text" name="title" value="<?php echo $arr[0]['title']?>" autofocus ></td>
        </tr>
        <tr>
            <td>Hobies:</td>
            <td>
                Play:<input type="checkbox" name="hobies[]" value="play" <?php if(in_array('play',unserialize($arr[0]['hobies']) )){echo "checked";}?>>
                Watch Movies:<input type="checkbox" name="hobies[]" value="watch Movies" <?php if(in_array("watch Movies",unserialize($arr[0]['hobies']))){echo "checked";} ?>>
                Facebooking :<input type="checkbox" name="hobies[]" value="Facebooking" <?php if(in_array("Facebooking",unserialize($arr[0]['hobies']))){echo "checked";} ?>>
                Gossiping :  <input type="checkbox" name="hobies[]" value="gossiping" <?php if(in_array("gossiping",unserialize($arr[0]['hobies']))){echo "checked";} ?>>
                Programming :<input type="checkbox" name="hobies[]" value="programming" <?php if(in_array("programming",unserialize($arr[0]['hobies']))){echo "checked";} ?>>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Update"></td>
        </tr>
    </table>
</form>
</body>
</html>
