<?php
include_once ("../../../vendor/autoload.php");

//--------------------class object call---------------
use App\modul1\hobies\Hobies;
$obj =new Hobies();
//---------------------Pagination---------------------
$totalItemPerPage = 5;
$totalRow = $obj->getNuberOfRow();
$totalPage = $totalRow/$totalItemPerPage;
$totalPage = ceil($totalPage);
if(isset($_GET['page'])){
    $currentPage = $_GET['page']-1;
}else{
    $currentPage = 0;
}
$offSet = $currentPage * $totalItemPerPage;


$arr = $obj->index($totalItemPerPage,$offSet);

//-------------------session--------------------------
if(isset($_SESSION['msg']) && $_SESSION['msg'] != ''){
    echo "!".$_SESSION['msg'];
    $_SESSION['msg'] = '';
}



/*echo "<pre>";
print_r($arr[0]);
die();*/
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Lab exam 5</title>
</head>
<body>
    <h1>Index Page</h1>
    <form action="search.php" method="get">
        <input type="text" name="title">
        <input type="submit" value="Search">
    </form>
    <a href="./create.php">Create your hobies.</a>
    <table border="1">
        <tr>
            <td>Name</td>
            <td>Hobies</td>
            <td colspan="2">Action</td>
        </tr>
        <?php foreach($arr as $t){ ?>
        <tr>
            <td><?php echo $t['title'] ?></td>
            <td>
                Play:<input type="checkbox" name="hobies[]" value="play" <?php if(in_array('play',unserialize($t['hobies']) )){echo "checked";}else{echo "disabled";}?>>
                Watch Movies:<input type="checkbox" name="hobies[]" value="watch Movies" <?php if(in_array("watch Movies",unserialize($t['hobies']))){echo "checked";}else{echo "disabled";} ?>>
                Facebooking :<input type="checkbox" name="hobies[]" value="Facebooking" <?php if(in_array("Facebooking",unserialize($t['hobies']))){echo "checked";}else{echo "disabled";} ?>>
                Gossiping :  <input type="checkbox" name="hobies[]" value="gossiping" <?php if(in_array("gossiping",unserialize($t['hobies']))){echo "checked";}else{echo "disabled";} ?>>
                Programming :<input type="checkbox" name="hobies[]" value="programming" <?php if(in_array("programming",unserialize($t['hobies']))){echo "checked";}else{echo "disabled";} ?>>
            </td>
            <td>
                <a href="show.php?id=<?php echo   $t['unique_id'] ?>">View</a>  |
                <a href="edit.php?id=<?php echo   $t['unique_id'] ?>">Edit</a>|
                <a href="trash.php?id=<?php echo $t['unique_id'] ?>">Delete</a>
            </td>
        </tr>
        <?php } ?>
    </table>
    <?php for ($i=1;$i<=$totalPage;$i++){ ?>
        &nbsp;<a href="./index.php?page=<?php echo $i ?>"><?php echo $i ?></a>
    <?php }?>
</body>
</html>