<?php

/*echo "<pre>";
print_r($_GET);
die();*/

include_once ("../../../vendor/autoload.php");

use App\modul1\hobies\Hobies;
$obj =new Hobies();

$arr = $obj->setData($_GET)->search();

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Lab exam 5</title>
</head>
<body>
<h1>Search Page</h1>
<a href="./index.php">Original List</a> |
<a href="./report/pdf.php?title=<?php echo $_GET['title'] ?>">PDF</a>
<table border="1">
    <tr>
        <td>Name</td>
        <td>Hobies</td>
    </tr>
    <?php foreach($arr as $t){ ?>
        <tr>
            <td><?php echo $t['title'] ?></td>
            <td>
                Play:<input type="checkbox" name="hobies[]" value="play" <?php if(in_array('play',unserialize($t['hobies']) )){echo "checked";}else{echo "disabled";}?>>
                Watch Movies:<input type="checkbox" name="hobies[]" value="watch Movies" <?php if(in_array("watch Movies",unserialize($t['hobies']))){echo "checked";}else{echo "disabled";} ?>>
                Facebooking :<input type="checkbox" name="hobies[]" value="Facebooking" <?php if(in_array("Facebooking",unserialize($t['hobies']))){echo "checked";}else{echo "disabled";} ?>>
                Gossiping :  <input type="checkbox" name="hobies[]" value="gossiping" <?php if(in_array("gossiping",unserialize($t['hobies']))){echo "checked";}else{echo "disabled";} ?>>
                Programming :<input type="checkbox" name="hobies[]" value="programming" <?php if(in_array("programming",unserialize($t['hobies']))){echo "checked";}else{echo "disabled";} ?>>
            </td>
        </tr>
    <?php } ?>
</table>
</body>
</html>