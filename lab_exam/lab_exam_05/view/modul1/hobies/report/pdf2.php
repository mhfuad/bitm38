<?php

include_once ("../../../../vendor/autoload.php");
include_once ("../../../../vendor/mpdf/mpdf/mpdf.php");
use App\modul1\hobies\Hobies;
$obj =new Hobies();

$arr = $obj->setData($_GET)->search();

/*echo "<pre>";
print_r($arr);
die();*/

$data = '';
foreach($arr as $t){
    $data .="<tr>";
        $data .="<td>".$t['title'] ."</td>";
        $data .="<td>".implode("", unserialize($t['hobies']))."</td>";
    $data .="</tr>";
}


$html = <<<EOD
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    
    <table border="1">
        <tr>
            <td>Name</td>
            <td>Hobies</td>
        </tr>
        <?php


        echo $data; ?>
    </table>
</body>
</html>
EOD;
$mpdf = new mPDF();
$mpdf->writeHTML($html);
$mpdf->Output();
