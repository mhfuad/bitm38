<?php

include_once ("../../../../vendor/autoload.php");
include_once ("../../../../vendor/tecnickcom/tcpdf/tcpdf.php");
use App\modul1\hobies\Hobies;
$obj =new Hobies();

$arr = $obj->setData($_GET)->search();

/*echo "<pre>";
print_r($arr);
die();*/
$data = '<h1>Hobies list.</h1>';
$data .= '<table border="1">
        <tr>
            <td><h4>Name</h4></td>
            <td><h4>Hobies</h4></td>
        </tr>';
foreach($arr as $t){
    $data .="<tr>";
    $data .="<td>".$t['title'] ."</td>";
    $data .="<td>".implode("", unserialize($t['hobies']))."</td>";
    $data .="</tr>";
}
$data .="</table>";


$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
$pdf->SetFont('times', 'BI', 12);
$pdf->AddPage();



$pdf->writeHTMLCell(0, 0, '', '', $data, 0, 1, 0, true, '', true);
$pdf->Output('hobies'.date('Y-m-d').'.pdf', 'I');