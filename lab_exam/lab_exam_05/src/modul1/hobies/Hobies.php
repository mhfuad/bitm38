<?php

namespace App\modul1\hobies;
use PDO;
class Hobies
{
    private $id;
    private $title;
    private $hobies;
    private $pdo ='';

    public function __construct()
    {
        session_start();
        //$this->pdo = new PDO('mysql:host=localhost;dbname=id485748_php38', 'id485748_fuad2', 'fuad2');
        $this->pdo = new PDO('mysql:host=localhost;dbname=php38', 'root', '');

    }
    public function setData($name = ''){
        if(array_key_exists('id',$name)){
            $this->id = $name['id'];
        }
        if(array_key_exists('title',$name)){
            $this->title = $name['title'];
        }
        if(array_key_exists('hobies',$name)){
            $this->hobies = $name['hobies'];
        }

        return $this;
    }
    public function store(){
        try {
            $query = "INSERT INTO `hobies` (`id`,`unique_id`,`title`,`hobies`)
              VALUES (:id,:unique_id,:title,:hobies)";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id' => null,
                    ':unique_id' => uniqid(),
                    ':title' => $this->title,
                    ':hobies' => $this->hobies,
                )
            );
            if($stmt){
                //echo "inser successfull";
                $_SESSION['msg'] = "Insert Successfully";
                header('location: index.php');
            }

        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function index($row,$offset)
    {
        try {
            $sql = "SELECT * FROM hobies WHERE `delete_at`='0000-00-00 00:00:00' LIMIT $row OFFSET $offset";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            //return $sql;
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function update(){
        try {
            $sql = "UPDATE `hobies` SET `title`=:title,`hobies`=:hobies WHERE `unique_id`=:unique";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute(
                array(
                    ':title'=>$this->title,
                    ':hobies'=>serialize($this->hobies),
                    ':unique'=>$this->id,
                )
            );
            if($stmt){
                header('location: index.php');
                $_SESSION['msg'] = "Update successfully";
            }
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function show()
    {
        try {
            $query = "SELECT * FROM `hobies` WHERE `unique_id`=:id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':id'=>$this->id,
                )
            );
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function delete()
    {
        try {
            $query = "UPDATE `hobies` SET `delete_at`=:delete WHERE `unique_id`=:unique";
            //$query = "SELECT * FROM `hobies` WHERE `unique_id`=:id";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':delete'=>date('Y-m-d h:m:s'),
                    ':unique'=>$this->id,
                )
            );
            if($stmt){
                header('location: index.php');
                $_SESSION['msg'] = "Delete successfully";
            }
            return $stmt->fetchAll();
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
    public function getNuberOfRow()
    {
        try {
            $sql = "SELECT * FROM hobies WHERE `delete_at`='0000-00-00 00:00:00'";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();
            return count($stmt->fetchAll());
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function search()
    {
        try {
            $query = "SELECT * FROM `hobies` WHERE `title`=:title and `delete_at`='0000-00-00 00:00:00'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(
                array(
                    ':title'=> $this->title,
                )
            );
            if($stmt){
                return $stmt->fetchAll();
            }
        } catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}