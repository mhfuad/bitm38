<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 12/14/2016
 * Time: 9:48 AM
 */


$var = 12.23;
echo $var;

$arr = array("fuad","hasan",23);
if(is_array($arr)){
    echo '$arr'.'is a array<br>';
}else{
    echo '$arr is not an array<br>';
}

$var2 = 12;
if(is_null($var2)){
    echo '$var2 is Null<br>';
}else{
    echo '$var2 is not Null<br>';
}

class a{

}
$aObject = new a();
if(is_object($aObject)){
    echo '$aObject is a object <br>';
}else{
    echo '$aObject is <strong>Not a </strong>object <br>';
}

$arr2 = array("BITM","BASIS");
echo "<pre>";
print_r($arr2);
echo "</pre>";
echo "<br>";

$arr3 = array('BITM',"PHP",38);
$after = serialize($arr3);
echo $after;
echo "<pre>";
unserialize($after);
echo "</pre>";
echo "<br>";
$a = 20;
echo '$a is seted value is :'.$a;
unset($a);
if(isset($a)){

    echo '$a is unseted value is :'.$a;
}else{
    echo '$a is not seted';
}
echo "<br>";

$b = true;
if(is_bool($b)){
    echo "is a booean Value";
}
echo "<br>";

$flotVal = 23.24234234;
if(is_float($flotVal)){
    echo '$flotVal is Float type';
}else{
    echo '$flotVal is not Float type';
}
echo "<br>";
