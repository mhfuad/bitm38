<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Form</title>

    <!-- Bootstrap -->
    <link href="../boot/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">

            <H1 class="center">Form</H1>
            <form action="calculater.php" method="get">
                <div class="form-group">
                    <label for="First Number">First Number</label>
                    <input type="text" class="form-control" placeholder="first name"  name="first_name" id="first_name">
                </div>
                <div class="form-group">
                    <label >Opprator</label>
                    <input type="text" class="form-control"  name="opparator" placeholder="Opparator">
                </div>
                <div class="form-group">
                    <label for="Last Number">Last Number</label>
                    <input type="text" class="form-control"  name="last_name" placeholder="last name">
                </div>
                <button type="button" class="btn btn-default" id="1">1</button>
                <button type="button" class="btn btn-default" id="2">2</button>
                <button type="button" class="btn btn-default" id="3">3</button>
                <button type="button" class="btn btn-default" id="4">4</button>
                <button type="button" class="btn btn-default" id="5">5</button><br>

                <button type="button" class="btn btn-default" id="6">6</button>
                <button type="button" class="btn btn-default" id="7"> 7</button>
                <button type="button" class="btn btn-default" id="8">8</button>
                <button type="button" class="btn btn-default" id="9">9</button>
                <button type="button" class="btn btn-default" id="0">0</button><br>

                <button type="button" class="btn btn-default" id="add">+</button>
                <button type="button" class="btn btn-default" id="sub">-</button>
                <button type="button" class="btn btn-default" id="mult">*</button>
                <button type="button" class="btn btn-default" id="div">/</button>
                <button type="submit" class="btn btn-lg btn-success" id="=">=</button>
            </form>
        </div>
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../boot/js/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../boot/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        $('#1').click(function(){
            $('#first_name').val(1);
        });
        $('#2').click(function(){
            $('#first_name').val(2);
        });
        $('#3').click(function(){
            $('#first_name').val(3);
        });
        $('#4').click(function(){
            $('#first_name').val(4);
        });
        $('#5').click(function(){
            $('#first_name').val(5);
        });
        $('#6').click(function(){
            $('#first_name').val();
        });
        $('#7').click(function(){
            $('#first_name').val(7);
        });
        $('#8').click(function(){
            $('#first_name').val(8);
        });
        $('#9').click(function(){
            $('#first_name').val()+9;
        });
        $('#0').click(function(){
            $('#first_name').val()+0;
        });
        $('#add').click(function(){
            $('#first_name').val()+"+";
        });
        $('#sub').click(function(){
            $('#first_name').val()+"-";
        });
        $('#mult').click(function(){
            $('#first_name').val()+"*";
        });
        $('#div').click(function(){
            $('#first_name').val()+"/";
        });
    });
</script>
</body>
</html>