<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Form</title>

    <!-- Bootstrap -->
    <link href="../boot/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">

            <H1 class="center">Form</H1>
            <form action="res.php" method="get">
                <div class="form-group">
                    <label >Bangla</label>
                    <input type="text" class="form-control"  name="bangla" id="first_name">
                </div>
                <div class="form-group">
                    <label >English</label>
                    <input type="text" class="form-control"  name="english" >
                </div>
                <div class="form-group">
                    <label >Math</label>
                    <input type="text" class="form-control"  name="math">
                </div>
                <div class="form-group">
                    <label >Scince</label>
                    <input type="text" class="form-control"  name="scince" >
                </div>

                <button type="submit" class="btn btn-lg btn-success" id="">Result</button>
            </form>
        </div>
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../boot/js/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../boot/js/bootstrap.min.js"></script>
</body>
</html>
